<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title>Tic-Tac-Toe. This is the title. It is displayed in the titlebar of the window in most browsers.</title>
    <meta name="description" content="Tic-Tac-Toe-Game. Here is a short description for the page. This text is displayed e. g. in search engine result listings.">
    <style>
        table.tic td {
            border: 1px solid #333; /* grey cell borders */
            width: 8rem;
            height: 8rem;
            vertical-align: middle;
            text-align: center;
            font-size: 4rem;
            font-family: Arial;
        }
        table { margin-bottom: 2rem; }
        input.field {
            border: 0;
            background-color: white;
            color: white; /* make the value invisible (white) */
            height: 8rem;
            width: 8rem !important;
            font-family: Arial;
            font-size: 4rem;
            font-weight: normal;
            cursor: pointer;
        }
        input.field:hover {
            border: 0;
            color: #c81657; /* red on hover */
        }
        .colorX { color: #e77; } /* X is light red */
        .colorO { color: #77e; } /* O is light blue */
        table.tic { border-collapse: collapse; }
    </style>
</head>
<body>
<section>
    <h1>Tic-Tac-Toe</h1>
    <article id="mainContent">
        <h2>Your free browsergame!</h2>
        <p>Type your game instructions here...</p>
        <form method="get" action="index.php">
            <?php
            session_start();

            $board = array(
                array("","",""),
                array("","",""),
                array("","",""));

            if(isset($_SESSION['board'])) {
                $board = unserialize($_SESSION['board']);
            }

            if(isset($_GET)) {
                $allKeys = array_keys($_GET);

                if(!empty($allKeys)) {

                    $key = $allKeys[0];

                    if(str_starts_with($key, "cell-")) {
                        $param = str_replace('cell-', '', $key);
                        $row = substr($param, 0, 1);
                        $column = substr($param, -1);

                        $board[$row][$column] = $_GET[$key];
                    }
                }
            }

            $_SESSION['board'] = serialize($board);

            $lastInput = 1;

            if(isset($_SESSION['lastInput'])) {
                $lastInput = unserialize($_SESSION['lastInput']);
            }

            $lastInput++;

            $_SESSION['lastInput'] = serialize($lastInput);

            echo '<table class="tic">'."\n";
            for ($i = 0; $i < count($board); $i++) {
                echo '<tr>'."\n";

                for ($j = 0; $j < count($board); $j++) {
                    if ($board[$i][$j] === "") {
                        //echo "\t".'<td><input type="submit" class="reset field" name="cell-'.$i.'-'.$j.'" value="X" /></td>'."\n";
                        echo "\t".'<td><input type="submit" class="reset field" name="cell-'.$i.'-'.$j.'" value="' . check($lastInput) . '" /></td>'."\n";
                    } else {
                        echo "\t".'<td><span class="color'.$board[$i][$j].'">'.$board[$i][$j].'</span></td>'."\n";
                    }
                }

                echo '</tr>'."\n";
            }
            echo '</table>'."\n";

            function check($number): string {
                if($number % 2 == 0) {
                    return "X";
                } else {
                    return "O";
                }
            }
            ?>
        </form>
    </article>
</section>
</body>
</html>